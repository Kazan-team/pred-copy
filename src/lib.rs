// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use std::fmt::Debug;

pub type MaskType = u64;
pub const MASK_BIT_COUNT: usize = (0 as MaskType).count_zeros() as usize;
pub const LOG2_MASK_BIT_COUNT: usize = MASK_BIT_COUNT.trailing_zeros() as usize;

macro_rules! static_assert {
    ($expr:expr, $name:ident) => {
        #[allow(dead_code)]
        const $name: () = [()][(($expr) != true) as usize];
    };
}

static_assert!(
    MASK_BIT_COUNT == 1 << LOG2_MASK_BIT_COUNT,
    ASSERT_MASK_BIT_COUNT_IS_POWER_OF_2
);

/// finds the index of the first one bit in `mask` starting from `index`
fn find_one_from(mask: MaskType, index: usize) -> Option<usize> {
    if index >= MASK_BIT_COUNT {
        return None;
    }
    let retval = (mask >> index).trailing_zeros() as usize + index;
    if retval >= MASK_BIT_COUNT {
        None
    } else {
        Some(retval)
    }
}

fn nth_bit_index(mask: MaskType, mut n: usize) -> Option<usize> {
    for i in 0..MASK_BIT_COUNT {
        if (mask & (1 << i)) != 0 {
            if n == 0 {
                return Some(i);
            }
            n -= 1;
        }
    }
    None
}

fn compute_index_from_other_index(
    mask: MaskType,
    other_mask: MaskType,
    other_index: usize,
) -> Option<usize> {
    if other_index >= MASK_BIT_COUNT {
        return None;
    }
    let n = other_mask.count_ones() as usize - (other_mask >> other_index).count_ones() as usize;
    nth_bit_index(mask, n)
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum PredCopyDone {
    Done,
    NotYet,
}

pub trait PredCopy: Debug {
    type Element: Copy;
    fn src(&self) -> &[Self::Element];
    fn dest(&mut self) -> &mut [Self::Element];
    fn src_mask(&self) -> MaskType;
    fn dest_mask(&self) -> MaskType;
    fn src_len(&self) -> usize;
    fn dest_len(&self) -> usize;
    fn src_index(&self) -> usize;
    fn dest_index(&self) -> usize;
    fn set_src_index(&mut self, value: usize);
    fn set_dest_index(&mut self, value: usize);
    fn next(&mut self) -> PredCopyDone {
        let src_len = self.src_len();
        let dest_len = self.dest_len();
        let src_index =
            find_one_from(self.src_mask(), self.src_index()).filter(|index| *index < src_len);
        let dest_index =
            find_one_from(self.dest_mask(), self.dest_index()).filter(|index| *index < dest_len);
        if let (Some(src_index), Some(dest_index)) = (src_index, dest_index) {
            let element = self.src()[src_index];
            self.dest()[dest_index] = element;
            self.set_src_index(src_index + 1);
            self.set_dest_index(dest_index + 1);
            PredCopyDone::NotYet
        } else {
            self.set_src_index(0);
            self.set_dest_index(0);
            PredCopyDone::Done
        }
    }
}

pub trait PredCopyMaker<T: Copy> {
    fn make<'a>(
        &self,
        src: &'a [T],
        src_mask: MaskType,
        dest: &'a mut [T],
        dest_mask: MaskType,
    ) -> Box<dyn PredCopy<Element = T> + 'a>;
}

#[derive(Debug)]
pub struct Input1<'a, T> {
    pub src: &'a [T],
    pub src_index: usize,
    pub src_mask: MaskType,
    pub src_len: usize,
    pub dest: &'a mut [T],
    pub dest_index: usize,
    pub dest_mask: MaskType,
    pub dest_len: usize,
}

pub struct Input1Maker;

impl<T: Copy + Debug + 'static> PredCopyMaker<T> for Input1Maker {
    fn make<'a>(
        &self,
        src: &'a [T],
        src_mask: MaskType,
        dest: &'a mut [T],
        dest_mask: MaskType,
    ) -> Box<dyn PredCopy<Element = T> + 'a> {
        Box::new(Input1 {
            src_len: src_mask.count_ones() as usize,
            src,
            src_index: 0,
            src_mask,
            dest_len: dest_mask.count_ones() as usize,
            dest,
            dest_index: 0,
            dest_mask,
        })
    }
}

impl<T: Copy + Debug> PredCopy for Input1<'_, T> {
    type Element = T;
    fn src(&self) -> &[Self::Element] {
        self.src
    }
    fn dest(&mut self) -> &mut [Self::Element] {
        self.dest
    }
    fn src_mask(&self) -> MaskType {
        self.src_mask
    }
    fn dest_mask(&self) -> MaskType {
        self.dest_mask
    }
    fn src_len(&self) -> usize {
        assert_eq!(self.src_mask.count_ones() as usize, self.src_len);
        self.src_len
    }
    fn dest_len(&self) -> usize {
        assert_eq!(self.dest_mask.count_ones() as usize, self.dest_len);
        self.dest_len
    }
    fn src_index(&self) -> usize {
        self.src_index
    }
    fn dest_index(&self) -> usize {
        self.dest_index
    }
    fn set_src_index(&mut self, value: usize) {
        self.src_index = value;
    }
    fn set_dest_index(&mut self, value: usize) {
        self.dest_index = value;
    }
}

#[derive(Debug)]
pub struct Input2<'a, T> {
    pub src: &'a [T],
    pub src_mask: MaskType,
    pub dest: &'a mut [T],
    pub dest_index: usize,
    pub dest_mask: MaskType,
    pub dest_len: usize,
}

pub struct Input2Maker;

impl<T: Copy + Debug + 'static> PredCopyMaker<T> for Input2Maker {
    fn make<'a>(
        &self,
        src: &'a [T],
        src_mask: MaskType,
        dest: &'a mut [T],
        dest_mask: MaskType,
    ) -> Box<dyn PredCopy<Element = T> + 'a> {
        Box::new(Input2 {
            src,
            src_mask,
            dest_len: dest_mask.count_ones() as usize,
            dest,
            dest_index: 0,
            dest_mask,
        })
    }
}

impl<T: Copy + Debug> PredCopy for Input2<'_, T> {
    type Element = T;
    fn src(&self) -> &[Self::Element] {
        self.src
    }
    fn dest(&mut self) -> &mut [Self::Element] {
        self.dest
    }
    fn src_mask(&self) -> MaskType {
        self.src_mask
    }
    fn dest_mask(&self) -> MaskType {
        self.dest_mask
    }
    fn src_len(&self) -> usize {
        self.src_mask.count_ones() as usize
    }
    fn dest_len(&self) -> usize {
        assert_eq!(self.dest_mask.count_ones() as usize, self.dest_len);
        self.dest_len
    }
    fn src_index(&self) -> usize {
        compute_index_from_other_index(self.src_mask, self.dest_mask, self.dest_index)
            .unwrap_or(MASK_BIT_COUNT)
    }
    fn dest_index(&self) -> usize {
        self.dest_index
    }
    fn set_src_index(&mut self, _value: usize) {}
    fn set_dest_index(&mut self, value: usize) {
        self.dest_index = value;
    }
}

#[derive(Debug)]
pub struct Input3<'a, T> {
    pub src: &'a [T],
    pub src_index: usize,
    pub src_mask: MaskType,
    pub src_len: usize,
    pub dest: &'a mut [T],
    pub dest_mask: MaskType,
}

pub struct Input3Maker;

impl<T: Copy + Debug + 'static> PredCopyMaker<T> for Input3Maker {
    fn make<'a>(
        &self,
        src: &'a [T],
        src_mask: MaskType,
        dest: &'a mut [T],
        dest_mask: MaskType,
    ) -> Box<dyn PredCopy<Element = T> + 'a> {
        Box::new(Input3 {
            src_len: src_mask.count_ones() as usize,
            src,
            src_index: 0,
            src_mask,
            dest,
            dest_mask,
        })
    }
}

impl<T: Copy + Debug> PredCopy for Input3<'_, T> {
    type Element = T;
    fn src(&self) -> &[Self::Element] {
        self.src
    }
    fn dest(&mut self) -> &mut [Self::Element] {
        self.dest
    }
    fn src_mask(&self) -> MaskType {
        self.src_mask
    }
    fn dest_mask(&self) -> MaskType {
        self.dest_mask
    }
    fn src_len(&self) -> usize {
        assert_eq!(self.src_mask.count_ones() as usize, self.src_len);
        self.src_len
    }
    fn dest_len(&self) -> usize {
        self.dest_mask.count_ones() as usize
    }
    fn src_index(&self) -> usize {
        self.src_index
    }
    fn dest_index(&self) -> usize {
        compute_index_from_other_index(self.dest_mask, self.src_mask, self.src_index)
            .unwrap_or(MASK_BIT_COUNT)
    }
    fn set_src_index(&mut self, value: usize) {
        self.src_index = value;
    }
    fn set_dest_index(&mut self, _value: usize) {}
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem;

    #[test]
    fn test_pred_copy() {
        let makers: &[&dyn PredCopyMaker<u8>] = &[&Input1Maker, &Input2Maker, &Input3Maker];
        let input_array = [1, 2, 3, 4];
        let output_array_template = vec![1, 2, 3, 4];
        for src_mask in 0..0x10 {
            for dest_mask in 0..0x10 {
                let results: Vec<_> = makers
                    .iter()
                    .map(|&maker| {
                        let mut output_array = output_array_template.clone();
                        let mut pred_copy =
                            maker.make(&input_array, src_mask, &mut output_array, dest_mask);
                        let mut iter_count = 0;
                        while pred_copy.next() == PredCopyDone::NotYet {
                            iter_count += 1;
                        }
                        mem::drop(pred_copy);
                        (iter_count, output_array)
                    })
                    .collect();
                let (first, rest) = results.split_first().expect("no makers");
                for result in rest {
                    assert_eq!(first, result);
                }
            }
        }
    }
}
